package com.example.microservices;

import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import lombok.extern.slf4j.Slf4j;
import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.RouteLocator;

import java.util.ArrayList;
import java.util.List;

@EnableDiscoveryClient
@EnableSwagger2Doc
@EnableZuulProxy
@Slf4j
@SpringCloudApplication
public class MicroservicesApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(MicroservicesApplication.class).run(args);
    }

    @Component
    @Primary
    class SwaggerConfig implements SwaggerResourcesProvider {
        @Autowired
        RouteLocator routeLocator;

        @Override
        public List<SwaggerResource> get() {
            List<SwaggerResource> resources = new ArrayList<>();

            routeLocator.getRoutes().forEach(route ->{
                resources.add(swaggerResource(route.getId(),route.getFullPath().replace("**", "docs/"), "2.0"));
            });

            return resources;
        }

        private SwaggerResource swaggerResource(String name,String location, String version) {
            SwaggerResource swaggerResource = new SwaggerResource();
            swaggerResource.setName(name);
            swaggerResource.setLocation(location);
            swaggerResource.setSwaggerVersion(version);
            return swaggerResource;
        }
    }
}
